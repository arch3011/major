package com.example.CollegeAutomation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CollegeAutomationApplication {

	public static void main(String[] args) {
		SpringApplication.run(CollegeAutomationApplication.class, args);
	}

}
